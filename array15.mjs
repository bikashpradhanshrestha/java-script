//sort according to the length of string
//ascending
// let list=["a","abcd","abc","dc","c"];
// let newlist=list.sort((a,b)=>{
//     return a.length-b.length;
// });
// console.log(newlist);
//decending
// let list=["a","abcd","abc","dc","b"];
// let newlist=list.sort((a,b)=>{
//     return b.length-a.length;
// });
// console.log(newlist);
//sort the object of array according the price
let products=[
    {    name:"earphone",price:1000 },
    {    name:"battery",price:2000 },
    {    name:"charger",price:500 },
]; 
let sortedProducts=products.sort((a,b)=>{
    return a.price - b.price;
});
console.log(sortedProducts);

